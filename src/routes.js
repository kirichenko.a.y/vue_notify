import { createRouter, createWebHistory} from 'vue-router'

import home from '@/pages/Home'
import notify from '@/pages/NotifyPage'

const routes = [
    { path: '/', name:'home', component: home },
    { path: '/notify', name:'notify', component: notify },
]

const router = createRouter({
    // mode: 'history',
    history: createWebHistory(),
    routes,
})

export default router
